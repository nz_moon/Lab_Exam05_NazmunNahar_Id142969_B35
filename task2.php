<?php
$startDate = new DateTime("1981-11-03");
$endDate = new DateTime("2013-09-04");

$date = $startDate->diff($endDate);
echo "Difference : " . $date->y . " years, " . $date->m." months, ".$date->d." days "."\n";
?>