<?php
class MyCalculator {
    public $num1, $num2;
    public function __construct( $number1, $number2 ) {
        $this->num1 = $number1;
        $this->num2= $number2;
    }
    public function add() {
        return $this->num1 + $this->num2;
    }

    public function subtract(){
        return$this->num1 - $this->num2;
    }

    public function multiply(){
        return$this->num1 * $this->num2;
    }

    public function divide(){
        return$this->num1 / $this->num2;
    }
}
$mycalc = new MyCalculator(12, 6);
echo "The Summation is :".$mycalc-> add()."<br>"; // Displays 18
echo "The Subtraction is :".$mycalc-> subtract()."<br>"; // Displays 6
echo "The Multiplication is :".$mycalc-> multiply()."<br>"; // Displays 72
echo "The Division is :".$mycalc-> divide()."<br>"; // Displays 2
?>